#ifndef EXERCISE_1_TREE_FUNCTIONS_H
#define EXERCISE_1_TREE_FUNCTIONS_H

#endif

typedef struct TreeNode {
    int key;
    struct TreeNode *left;
    struct TreeNode *right;
} TreeNode_t;

TreeNode_t *newTreeNode(int key);

TreeNode_t *insertNode(TreeNode_t *node, int key);

TreeNode_t *findNode(TreeNode_t *root, int key);

void inorder(TreeNode_t* node);