#include <stdio.h>
#include <stdlib.h>
#include "tree_functions.h"


TreeNode_t *newTreeNode(int key) {
    TreeNode_t *node = (TreeNode_t *) malloc(sizeof(TreeNode_t));
    node->key = key;
    node->left = NULL;
    node->right = NULL;
    return node;
}


/* input: the node to which we want to add a new one, value of new node
   output: pointer to new node on the right or left side */
TreeNode_t *insertNode(TreeNode_t *node, int key) {
    if (node == NULL) {
        node = newTreeNode(key);
    } else if (key < node->key) {
        node->left = insertNode(node->left, key);
    } else {
        node->right = insertNode(node->right, key);
    }
    return node;
}




TreeNode_t *findNode(TreeNode_t *root, int key) {
    if (root == NULL || root->key == key) {
        return root;
    } else if (key < root->key) {
        return findNode(root->left, key);
    } else {
        return findNode(root->right, key);
    }
}




void inorder(TreeNode_t* node) {
    if (node == NULL) {
        printf("null");
        return;
    }
    else {
        printf("%d", node->key);

        printf(",");
        inorder(node->left);

        printf(",");
        inorder(node->right);

    }
}


