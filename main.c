#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tree_functions.h"



int main(int argc, char *argv[]) {
    int key; // variable fot items in nodes


    /* Checking number of arguments */
    if (argc != 3) {
        printf("Wrong number of arguments");
        return 1;
    }

    /* Opening file and checking it for errors */
    FILE *file = fopen(argv[1], "r");
    if (file == NULL) {
        printf("Unable to open file %s\n", argv[1]);
        return 1;
    }

    /* Creating pointer to node, root and defining NULL to it*/
    TreeNode_t *node = NULL;


    /* Working with file: getting size of file and string from it */
    int size; // variable for size of file
    fseek(file, 0, SEEK_END);
    size = ftell(file);
    fseek(file, 0, SEEK_SET);


    char stringOfNumbers[size / sizeof(char)]; // string with numbers for strtok function
    fgets(stringOfNumbers, 100, file);

    fclose(file);


    /* Working with string: */
    char *number = strtok(stringOfNumbers, ","); // taking first number from string
    key = atoi(number);

    node = newTreeNode(key); // creating root


    /* Filling the tree with nodes */
    while (number != NULL) {
        number = strtok(NULL, ",");
        key = atoi(number);
        if (key != 0) {
            insertNode(node, key);
        }
    }


    TreeNode_t *nodeForSearch = findNode(node, atoi(argv[2]));
    if (nodeForSearch == NULL) {
        printf("Node with value %d not found in the tree\n", atoi(argv[2]));
        return 1;
    } else {
        inorder(nodeForSearch);
    }
    return 0;
}